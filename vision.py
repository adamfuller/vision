import numpy as np
import matplotlib.pyplot as plt
import skimage.io
import skimage.data
from skimage.feature import blob_dog, blob_log, blob_doh
import VideoCapture

print "Done importing."

plt.close('all')

c = VideoCapture.Device(0)

i = 5

image = c.getImage()


image = np.array(image.getdata()).reshape(image.size[1], image.size[0], 3)

image = 1 - image

image = image.mean(axis=2)/255.

i -= 1

print "One image captured."

plt.imshow(image, interpolation='nearest')

print "Image displayed."

blobs = blob_log(image, min_sigma=5, max_sigma=30, num_sigma=10, threshold=.05)

print str(len(blobs))+" blobs found."

if 0:
	ib = 0
	for b in blobs:
		print "Showing Blob #"+str(ib)
		c = plt.Circle(b[[1,0]], radius=b[2]*np.sqrt(2)*2, fill=False, color='yellow')
		plt.gca().add_patch(c)
		ib += 1
else:
	plt.scatter(blobs[:,1], blobs[:,0], s=blobs[:,2]*1.4*200, marker='*',
			facecolors='none', linewidth=2)
